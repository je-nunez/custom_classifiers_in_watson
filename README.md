
# Description

This repository has two different programs, separated in two distinct subfolders in this project, **./visual_recognition_cancer_surgery/** and **./jane_jacobs_urban_planning/**. The specific details, requirements, and information, are not in this brief introduction, but are in the READMEs files in the programs' subfolders.

# Visual Recognition of Contours of Cancer Zones

One program in this repository does a visual recognition useful in cancer **surgery** (and **all surgeries in general** where visual recognition can be useful.).

The background of this program is that I had to do an exposition several months ago where it was analyzed the increase in the cost of personal health care. An introduction to this issue is: https://en.wikipedia.org/wiki/Health_insurance_costs_in_the_United_States

The program trains Watson to learn how a cancer looks like from some small visual samples of it, and after being trained in this **new, custom visual classifier**, then this program asks Watson to find on an input image of the **entire human organ** where the cancer is, masking those areas which are not affected by the cancer and exposing those which are, similar to the visual examination that a surgeon does on the organ surface.

In a formal description, it tries to find the edges (or contours) of the cancer on the human organ, like for guiding an scalpel.

This example works on this base image:

![Pancreatic Cancer affecting the liver](visual_recognition_cancer_surgery/pancreatic/cl.jpg "Pancreatic Cancer affecting the liver")

(which is found here: https://commons.wikimedia.org/wiki/File:Secondary_tumor_deposits_in_the_liver_from_a_primary_cancer_of_the_pancreas.jpg )

and then produces a resulting image, using only the scores of the visual classification returned by Watson, that focus only where the cancer is, masking out all the other area of the organ:

If the division of the image of the organ is in **10x10 subtiles**:

![Image of the liver focusing only on the cancer, 10x10 subtiling](visual_recognition_cancer_surgery/result_10_x_10_tiles.png "Image of the liver focusing only on the cancer, 10x10 subtiling")

If the division of the image of the organ is in **30x30 subtiles**:

![Image of the liver focusing only on the cancer, 30x30 subtiling](visual_recognition_cancer_surgery/result_30_x_30_tiles.png "Image of the liver focusing only on the cancer, 30x30 subtiling")

** There are more details and information in that program's subfolder, *./visual_recognition_cancer_surgery/README.md* file**

# Jane Jacobs Urban Planning

**This program is to try Watson to learn Urban Planning, which includes quantification of beauty, like in formal color theory, and other constrains of Urban Planning, like financial.

The subcomponent of formal color theory has a different aspect, and it is quantification because it has different quantification of values for Urban Planning. E.g., for an Urban Planner these three sequences represent different values.

![An ordered sequence of colors] (jane_jacobs_urban_planning/an_example_of_color_sub_wheel_to_learn_01.png)
![An ordered sequence of colors] (jane_jacobs_urban_planning/an_example_of_color_sub_wheel_to_learn_02.png)
![An ordered sequence of colors] (jane_jacobs_urban_planning/an_example_of_color_sub_wheel_to_learn_03.png)

The background of this is this map, the same map, different colors. One it is more difficult to keep readin than the other.

![Same map](jane_jacobs_urban_planning/Toronto_Transit_Black_Background.jpg)
![Same map](jane_jacobs_urban_planning/Toronto_Transit_White_Background.jpg)

