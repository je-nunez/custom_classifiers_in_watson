#!/usr/bin/env python2

"""
Watson as an Urban Planner.

The problem posted by Jane Jacobs on Urban Planning, on optimization of
___diversity and variety___, because statistics seems to point out that the
death of modern urban cities is ___monotony___, so to optimize monotony is to
eradicate the life of urban cities. Hence, the Urban Planner should increase
entertaining diversity and variety (as in TV, but it can be wise while doing
so), not decrease it. Hence, to see Watson working on this issue and also in
art appreciation and formal color theory.

But, only with part of likeability, then this expands as to whether Watson
can understand books like "The Semiology of Graphics: Diagrams, Networks,
Maps", by Jacques Bertin, and "The Grammar of Graphics", by Leland Wilkinson.

This issue with Watson as an urban planner is related to sculpture and
3D-printing with the SolidPython or another 3D-Printing module (we'll
simulate it with the Blender module -https://www.blender.org/-), not only to
Tradeoff Analytics of the counfounding variables of pros, cons, and risks of
executing certain urban development (e.g., the Tradeoff Analytics taking into
account the financial funds available, the expected revenues, political
valuation and other multivariate analysis).
"""

import sys
import os
import logging
from zipfile import ZipFile

sys.path.append('/Library/Python/2.7/site-packages/')

from watson_developer_cloud import VisualRecognitionV2Beta as VisualRecognition


class Config(object):
    """
       A few globals variables used by the program, represented as static
       field members of this Python class.
    """

    # pylint: disable=too-few-public-methods

    # The format of the name that the new custom Watson Visual Recognition
    # classifiers will have:

    watson_vr_new_classifiers = "likeability_color_wheel_{}"


def main():
    """Main function."""

    import argparse
    from argparse import RawDescriptionHelpFormatter

    detailed_usage = get_this_script_docstring()

    summary_usage = 'Tries to teach Watson how to be an urban planner by ' \
                    'training it in formal color theory, quantifying the ' \
                    'beauty in different color schemes, and the applying ' \
                    'the necessary Tradeoff Analytics that an urban ' \
                    'planner needs to have always in mind.'

    parser = argparse.ArgumentParser(description=summary_usage,
                                     epilog=detailed_usage,
                                     formatter_class=\
                                                  RawDescriptionHelpFormatter)

    # Note that this program requires two different Watson services, Visual
    # Recognition by the artistic part of Urban Planning, and Tradeoff
    # Analytics, by the financial and other pausability of an urban development
    # project.

    parser.add_argument('-u', '--username_vr',   # for Visual Recognition
                        required=True, type=str, metavar='username_vr',
                        help='The Bluemix username to connect to the Watson '
                             'Visual Recognizer front-end')

    parser.add_argument('-p', '--password_vr',   # for Visual Recognition
                        required=True, type=str, metavar='password_vr',
                        help='The Bluemix password to connect to the Watson '
                             'Visual Recognizer front-end')

    parser.add_argument('-a', '--username_ta',   # for Tradeoff Analytics
                        required=True, type=str, metavar='username_vr',
                        help='The Bluemix username to connect to the Watson '
                             'Tradeoff Analytics front-end')

    parser.add_argument('-s', '--password_ta',   # for Tradeoff Analytics
                        required=True, type=str, metavar='password_vr',
                        help='The Bluemix password to connect to the Watson '
                             'Tradeoff Analytics front-end')

    args = parser.parse_args()

    # configure logging ('level' should have been taken from the command-line)
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    # Obtain your credentials using:
    #    `cf env <the-name-of-my-watson-bluemix-visual-recognition-app>`
    logging.debug("Connecting to the Watson Visual Recognition broker")
    visual_recogn = VisualRecognition(version='2015-12-02',
                                      username=args.username_vr,
                                      password=args.password_vr)
    logging.debug("Connected. Training new classifiers on colors wheels")

    # Train Watson in the likeability (sentiment analysis) derived from
    # different color combinations (color schemes or wheels) because
    # Urban Planing needs to respect as well the aesthetics of art.

    train_watson_in_color_theory(visual_recogn)


def train_watson_in_color_theory(visual_recogn):
    """Color theory has long been established, since Isaac Newton.

        Probably Jane Jacobs, as urban planner, would have agreed on harmonies
        among specific clusters of colors, because urban planning includes
        art, it doesn't ignore it. (In the specialized literature in urban
        planning it is not called "cluster" of colors, as in Machine Learning,
        but "wheel" of colors.)

        The NASA has a specific web-page only on colors:
             http://colorusage.arc.nasa.gov/
        and color science:
             http://colorusage.arc.nasa.gov/color_science.php

        Here it is shown the clusters of color harmonies:
             https://designschool.canva.com/color-theory/

        Each cluster of color harmony has an artistic beauty level
        (sentiment), which Watson should learn. Beauty sentiment 100 is the
        highest and 1 is the lowest.

        The purpose of this method is to create the classifiers in Watson's
        Visual Recognition to associate the colors with the level (sentiment)
        of artistic beauty.

        (Furthermore, this quantification of artistic beauty is feed also into
         the Tradeoff Analytics, because an urban planner deals not only with
         art in the urban development, but also with other numerical
         constraints to optimize.)
    """

    # Let's say as an example that we prefer the "analogous wheel of colors",
    # then the "complementary", then the "monochromatic", but we don't like
    # the "split-complementary". (This is an example, there are many more
    # combinations or colors schemes, and of course, an urban planner can say
    # that two wheels of colors have the same sentiment of beauty, and then
    # the tradeoff-decision needs to be based on other confounding variables,
    # risks, financial costs, etc.
    #
    # (These preferences among clusters of colors are arbitrary and depend on
    #  tradition, human factors, etc. In general, it is what it is known as
    #  color theory, but it could be learned real-time from sentiment
    #  analysis. E.g., if the computer were fast enough, it could understand
    #  what it is being typed on the keyboard -by sentiment analysis- and then
    #  blush or control emotions of anger, etc, as in parental supervision,
    #  following the color wheel recommended by the Watson recommender -e.g.,
    #  for anger it would be to blush the keyboard with the colors you most
    #  like to calm you down - if each key on the keyboard had the luminous
    #  hardware making it able to blush. But our purpose is Jane Jacobs' urban
    #  planning, although it is less practical and more theoretical.)

    likeability_of_colors_wheels = [
        {
            'sentiment': 100, # analogous wheel of colors on green
            'color_wheel': ['#9FA838', '#F5FBA8', '#629833', '#BCE498',
                            '#8CBE5F', '#407213', '#244C00', '#256F5B',
                            '#6FA798', '#468B78', '#0E5440', '#003828'],
            'save_to_fname': 'analog_green_wheel.zip'
        },
        {
            'sentiment': 75, # complementary wheel of colors on red
            'color_wheel': ["Red", "MediumVioletRed", "Violet", "BlueViolet",
                            "Blue", "#307D7E", "Green", "GreenYellow",
                            "Yellow", "#FFDB58", "Orange", "OrangeRed"],
            'save_to_fname': 'complementary_red_wheel.zip'
        },
        {
            'sentiment': 50, # monochromatic wheel of colors on red
            'color_wheel': ["GreenYellow", "Chartreuse", "LawnGreen", "Lime",
                            "LimeGreen", "PaleGreen", "LightGreen",
                            "MediumSpringGreen", "SpringGreen",
                            "MediumSeaGreen", "SeaGreen", "ForestGreen",
                            "Green", "DarkGreen"],
            'save_to_fname': 'monochromatic_green_wheel.zip'
        },
        {
            'sentiment': 25, # split-complimentary wheel of colors on GrBlYlw
            'color_wheel': ['#2A7E43', '#832C65', '#AAA239', '#7EBD91',
                            '#C583AE', '#FFF9AA', '#4F9D66', '#A45287',
                            '#D4CD6A', '#105E27', '#621046', '#807815'],
            'save_to_fname': 'split_complement_green_blue_yellow_wheel.zip'
        }
        # ... more sentiments on other colors schemes. Zip filenname is only
        # for the temporary file to upload as training input to the Watson
        # custom classifier.
    ]

    # how many colors in a sub-wheel of a color wheel to learn
    watson_length_of_sub_wheel = 4

    logging.debug("Creating all dominos of color wheels and their sentiments")
    for likeabil_wheel in likeability_of_colors_wheels:
        sub_wheels = build_seq_color_sub_wheels(likeabil_wheel['color_wheel'],
                                                watson_length_of_sub_wheel)
        generate_dominos_zip_file(sub_wheels, likeabil_wheel['save_to_fname'])
    logging.debug("Prepared all dominos of color wheels and their sentiments")

    for likeabil_wheel_idx in xrange(len(likeability_of_colors_wheels)):
        train_likeabilit_of_color_wheel(likeabil_wheel_idx,
                                        likeability_of_colors_wheels,
                                        visual_recogn)


def train_likeabilit_of_color_wheel(wheel_idx, likeability_of_wheels,
                                    watson_visual_rec):

    """Trains Watson's Visual Recognition to recognize a certain given
    color wheel -given by its index 'wheel_idx'-, among other color
    wheels in given in the list 'likeability_of_wheels'."""

    # the name of the new Watson Visual Recognition classifier to learn
    new_classifier = Config.watson_vr_new_classifiers.format(wheel_idx)

    # the traininng sets for the new classifier
    positive_tr_fname = likeability_of_wheels[wheel_idx]['save_to_fname']
    negative_tr_fname = "negative_set_for_{}.zip".format(wheel_idx)

    try:
        with ZipFile(negative_tr_fname, 'w') as negative_zip:
            # build the negative training set from all other wheels
            for i in xrange(len(likeability_of_wheels)):
                if i == wheel_idx:
                    continue

                with ZipFile(likeability_of_wheels[i]['save_to_fname'], 'r') \
                    as other_zip:
                    for fname in other_zip.namelist():
                        negative_zip.writestr(fname,
                                              other_zip.open(fname).read())

        with open(positive_tr_fname, 'rb') as positive_tr, \
             open(negative_tr_fname, 'rb') as negative_tr:
            # ask Watson to learn this new classifier
            logging.debug("Training new classifier %s", new_classifier)
            results = (
                watson_visual_rec.create_classifier(name=new_classifier,
                                                    positive_examples=positive_tr,
                                                    negative_examples=negative_tr)
            )
            logging.debug("Training results %s", repr(results))
    finally:
        # if os.path.isfile(negative_tr_fname): os.remove(negative_tr_fname)
        pass


def generate_dominos_zip_file(colors_harmony, zip_file):
    """From an ordered sequence of colors in a color harmony (or wheel) -given
       in the ordered sequence 'colors_harmony', it tries to generate all
       training dominos having these ordered sequence of colors in this
       harmony, and collects them into a new Zip archive named 'zip_file'.
    """

    import glob

    # Two small, inner utility functions to this generate_dominos_zip_file()

    def zip_dominos_curr_color_harmony(domino_fname_preffix, zip_file):

        """Zip all the dominos and their variations -by shifting- in this
           color harmony."""

        write_mode = "a" if os.path.isfile(zip_file) else "w"
        with ZipFile(zip_file, write_mode) as zip_for_training_watson:
            for domino_fname in glob.glob(domino_fname_preffix + "*.png"):
                zip_for_training_watson.write(domino_fname)
                os.remove(domino_fname)


    def rm_all_dominos_in_color_harmony(domino_fname_preffix):
        """Remove all dominos and their variations -by shifting- in this
           color harmony."""

        for domino_fname in glob.glob(domino_fname_preffix + "*.png"):
            os.remove(domino_fname)


    # Code of generate_dominos_zip_file():

    # clean-up an old zip file with this same name, if it exists
    if os.path.isfile(zip_file):
        os.remove(zip_file)

    for colors_seq in colors_harmony:
        domino_fname_preffix = "_".join(colors_seq)
        try:
            generate_dominoes(colors_seq, domino_fname_preffix)
            zip_dominos_curr_color_harmony(domino_fname_preffix, zip_file)
        except Exception as dummy_exc:
            rm_all_dominos_in_color_harmony(domino_fname_preffix)
            raise


def build_seq_color_sub_wheels(color_wheel, length_of_sub_wheel):
    """Returns all possible consecutive ordered sequence (list) of length
       'length_of_sub_wheel' from the given 'color_wheel' (a circular
       list)
    """

    result = []
    len_wheel = len(color_wheel)
    for i in xrange(len_wheel):
        sub_wheel = [color_wheel[(i+j) % len_wheel]
                     for j in xrange(length_of_sub_wheel)]
        result.append(sub_wheel)
    return result



def generate_dominoes(colors_seq, dest_domino_preffix):
    """Generates a set of dominos .PNG files, all having this same color
       sequence 'colors_seq' from left to right, but differing among them,
       not in the colors, but only in the width of each color bar in the
       domino. This necessary to increase the dimensionality of the tensor
       for reinforcement learning. All dominos are saved with a common
       preffix 'dest_domino_preffix'.

       It is in this procedure that a better access to the underlying Tensor
       library might be nice, as to avoid training the neural network with a
       multitude of pixels in PNGs, but only with color names, e.g., the
       ordered harmony 'Blue, Green, and Yellow' can have a high level of
       sentiment of beauty. On the other hand, custom classifiers in
       Natural Language Processing don't seem easy because not all colors
       have English names, like "#307D7E", because the color wheels are
       continuous discrete RGB values. Hence, the Visual Recognition API
       seems to be the closest one to the Tensor library, although this is
       very inefficient. (This is the early sketch of this program.)
    """

    # These are the minimal dimensions recommended for an image in Watson's
    # Visual Recognition: 320 x 320. We'll use 500 x 500 as to give room
    # for width variation to reinforce the learning.
    # https://www.ibm.com/smarterplanet/us/en/ibmwatson/developercloud/doc/visual-recognition/customizing.shtml

    from random import shuffle
    from PIL import Image, ImageDraw

    domino_dims = (500, 500)

    uniform_width_of_each_bar = int(domino_dims[0] / len(colors_seq))

    variation_to_reinforce_learning = 50   # we'll generate 50 variations of
                                           # this domino as per URL above. Of
                                           # course, a better access to the
                                           # tensor library would avoid us to
                                           # generate domino PNGs as training
                                           # input, which is very inefficient.
                                           # (I sincerely apologize for this,
                                           # I would have liked to avoid this
                                           # and something more optimal, but
                                           # it is the rush for the hackathon)

    # the shifts in the column widths of each color bar inside the domino
    shifts_to_reinforce_learning = list(
        range(int(-variation_to_reinforce_learning/2),
              int(variation_to_reinforce_learning/2) + 1)
    )

    shuffle(shifts_to_reinforce_learning)

    for variation in xrange(len(shifts_to_reinforce_learning)):
        dest_domino_fname = "{}_{}.png".format(dest_domino_preffix, variation)
        curr_column_width = (uniform_width_of_each_bar +
                             shifts_to_reinforce_learning[variation])

        a_domino = Image.new('RGB', domino_dims)
        draw = ImageDraw.Draw(a_domino)

        current_column = 0

        for color in colors_seq:
            # paint this bar on the domino with the current color in colors_seq
            next_col = current_column + curr_column_width
            draw.rectangle([(current_column, 0), (next_col, domino_dims[1])],
                           outline=color, fill=color)
            current_column = next_col

        # paint the remainder bar in the domino with the last color, if any
        # remainder bar was left because of the shift on the uniform width
        if current_column != domino_dims[0] - 1:
            draw.rectangle([(current_column, 0), domino_dims],
                           outline=colors_seq[-1], fill=colors_seq[-1])

        a_domino.save(dest_domino_fname, 'PNG')


def get_this_script_docstring():
    """Utility function to get the Python docstring of this script"""
    import inspect

    current_python_script_pathname = inspect.getfile(inspect.currentframe())
    dummy_pyscript_dirname, pyscript_filename = \
                os.path.split(os.path.abspath(current_python_script_pathname))
    pyscript_filename = os.path.splitext(pyscript_filename)[0]  # no extension
    pyscript_metadata = __import__(pyscript_filename)
    pyscript_docstring = pyscript_metadata.__doc__
    return pyscript_docstring


if __name__ == '__main__':
    main()
