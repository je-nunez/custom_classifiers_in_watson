
# Description

The program trains Watson to learn how a cancer looks like from some small visual samples of it, and after being trained in this new, custom visual classifier, then it asks Watson to find on an input image of the entire human organ where the cancer is, masking those areas which are not affected by the cancer and exposing those which are, similar to the visual examination that a surgeon does on the organ surface.

In a formal description, it tries to find the edges (or contours) of the cancer on the human organ.

(Note: If a program were needed to assist a surgeon incorporating samples and biopsias, i.e., taking also into account real-time numerical data like systolic vs diastolic blood pressure, heart rate, etc., then this program is of not use for this field, because it tries to do **visual** recognition, not numerical regression or classification. I have another project which tries to work on this numerical data, i.e., in the measures of some distinct biomarkers of the liver, https://github.com/je-nunez/testing_WEKA_ml_on_BUPA, by using a random forest. The very first version of the project tried to use, though, a Multilayer Perceptron:

https://github.com/je-nunez/testing_WEKA_ml_on_BUPA/commit/70283a3cc6a401f83c15c491400859df3d69d37d#diff-16c8d3c0eae7e082fc0778af292d427d

The liver is a very essential and complex organ in the human body, and can require a lot of freedom in input and output tensors, so for lack of time and of more verification observational samples, it was decided for a random forest, but a neural network of course can classify it too.)

# Requirements

This program requires the Python `watson_developer_cloud` module:

      pip install --upgrade watson-developer-cloud

      ( https://pypi.python.org/pypi/watson-developer-cloud )

This program requires the Python `image_slicer` module (it will also install by default the Python Imaging Library, PIL):

      pip install --upgrade image_slicer

      ( https://pypi.python.org/pypi/image_slicer )

This program requires the Watson-Developer-Cloud Visual Recognition broker to be running in Bluemix: this requirement can be found here, and needs to be installed to run in Bluemix:

    https://github.com/watson-developer-cloud/visual-recognition-nodejs

# Expected Inputs and Results:

Receives as input some visual samples of cancer on a real human tissue and **dissects** the main human organ of which these visual samples belong to find the regions of this organ which have signs of cancer, and find the geometrical countours (edges) of this cancer in that organ, in the same way that a surgeon needs to do an optical recognizance of the organ to know on which countours (edges) to introduce the scalpel and start removing.

It works by constructing a new visual classifier with Watson Visual Recognition to know what tissue is visually cancer (and needs to be extirpated) and what is not.

As sample input, it takes this picture of a human organ (a liver from primary cancer of the pancreas):

https://commons.wikimedia.org/wiki/File:Secondary_tumor_deposits_in_the_liver_from_a_primary_cancer_of_the_pancreas.jpg

and some positive and negative visual biopsias of it as samples of what is visually cancer and what is not. This figure and small subsamples are already downloaded in the **./pancreatic/** subfolder here, with filename 'cl.jpg' for the main picture of the liver, and a few sections in that subfolder like 'healthy....jpg' and 'pancreatic_cancer....jpg' to build the new Watson classifier as to what is visually cancer (both are grouped in two zip files, 'th.zip' and 'tc.zip', with these visual subsamples of what looks healthy and what looks cancerigenous.

# How it Works:

The program **receives as a command-line argument the number of subtiles** into which to slice the image of the human organ -the liver in this case- and then, once that Watson has been trained in the new visual classiffier, slices the image into those many subtiles, creates a new temporary zip archive with all these generated subtiles, and submits it to Watson to be classified.

Once Watson answers with the score of each dinamycally sliced subtile, then the program masks the color of those subtiles which are healthy (Watson score = 0) and **for all the others subtiles, lights or shades them according to the score Watson returned for them**, using the alpha composite of the image.

It then rejoins all the subtiles back into the image of the organ with only cancerigenous subtiles clearly shown, and then finds the countours (edges) of these visually cancerigenous zones in the human organ.

It then deletes all dinamycally generated slices and zip file used for this analysis.

# Contraints:

The execution-time seems to be linear in respect to the number of subtiles into which to slice the image of the human organ, and **TCP timeouts** with Bluemix are possible. Optimistic execution-time is about a few minutes, but I have seen executions times of several hours followed by a TCP timeout. (The number of subtiles is quadratic, so when you ask to slice the visual image of the organ in 40 subtiles, the program slices the image horizontally and vertically in 40x40 = 1600 subtiles with identical dimensions, which are then submitted via a temporary zip file into Watson. If the program is requested to slice the image in 50 subtiles, it sends 2500 subtiles which equal dimensions to Watson. Of course, more subtiles is a finer slicing and a better precision for the countours (edges) advised to the surgeon, who do get tired in long surgeries. -When we refer to the number of subtiles we refer to the number after slicing, which is the square of the input parameter 'number_of_subtiles'-.

The maximum limit of subtiles is 99, ie., 99 x 99 subtiles sent to Watson. This is a limitation of this client program, not of Watson.

The program works on a static image of a human organ, and, since it slices it and sends them to a neural network for classification, then **it needs high-resolution images**, otherwise the sliced subtiles would have too few pixels is the image of the organ is in low resolution. In our case, the image of the liver is 2,784 × 1,884 pixels (I haven't tried 99x99 slicing it, too slow).

# Feature requests:

Since all the sliced subtiles are sent at once to Watson, a feature would be to have each subtile classified in parallel by an independent neural network, and this would cut the execution time of the program. (The hyperparameters, axons' weights, neurons input biases, etc, can be learned by training a 'leader' neural network with the same architecture, which is the one that initially is trained under the positive and negative examples of the new visual classifier. Once this leader has learned this new classifier of what is cancer, then this 'leader' neural network is cloned into 99x99 identical ones to classify in parallel each actual received subtile.

Another feature (this is less of a feature and probably more difficult) is that the image slicing and contours analysis be done in the server-side, i.e., the client sends to Bluemix the source image of the human organ and the number of subtiles it needs and then the server slices it, classifies each subtile, repaints them according to the classification score and joins them back in a resulting image of the organ. In this case, the client is more passive. (The idea of this feature is that, by incorporating this functionality, the server should do contour (edge) analysis of those sections of the image where the classifier changes in value, so the client only asks "*answer me the contours of this classifier*". This is less difficult than it sounds since **countour -edge- analysis is lineal algebra if it is done pixel-by-pixel; see, for example, the 3x3 lineal matrices used in the Python Imaging Library -PIL: Pillow should also use them- to find contours and edges**:

    find the 3x3 matrix below "class FIND_EDGES(BuiltinFilter):":

         http://www.sourcecodebrowser.com/python-imaging/1.1.7/_image_filter_8py_source.html

Of course, this is to find edges in pixels, where no classification is needed: Watson can recognize different adjacent zones -by doing clustering of similar pixels together-, and then, once the adjacent zones are found, apply the 3x3 lineal matrix to find edges and contours of those zones, not pixels.)

This is also can be used in mobile phones, because these devices require real-time answers, a phone can't be left waiting.

# Other possible uses:

In a similar way as the image of an organ is sliced and then scored by a custom neural network classifier, the image of an ecological disaster can be sliced into several subtiles to know which containment measures to take on each subtile. For example, in *oil spills*:

      http://www.treehugger.com/environmental-policy/us-not-prepared-major-oil-spill-arctic.html

Scoring each subtile for directing the positioning of bioremediants, dispersants, or the positioning or more appropiate techniques, by combining also sensors which predict the direction and force of the water surface flows, etc. (The later falls more properly in Tradeoff-Analytics than in visual recognition since these sensors can give the speed of the water flow in each GPS location, etc. For example, the National Oceanic and Atmospheric Administration has a system that models how different types of oil undergo physical and chemical changes in the marine environment:

      http://response.restoration.noaa.gov/adios

but this system can not locate visually each of these different types of oils in an actual spill. If images with sufficient resolution are feed into Watson, probably it could score on custom visual classifiers (locate) those subtiles where *each type of oil* is, and where it is being led by the currents.

In another similar ecological case, it is to deal with consumerism. Matter is not created or destroyed, but only transformed, so an increase in consumerism means more difficulty in properly and economically separating the residues of this consumerism, e.g., economically classifying aluminium from copper, from plastic, etc., by using the visual refraction indices at a short or medium distance. This is a similar visual classification as the one done above. Because, as consumerism increases, then promptly and easily classifying its residues is both an economical desire to re-use without using non-renovable resources, and ecological as well, because it pre-empts artificial micro-environments with disturb natural life, by a rapid classification and re-use.

