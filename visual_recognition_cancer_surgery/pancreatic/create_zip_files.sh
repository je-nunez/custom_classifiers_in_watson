#!/usr/bin/env sh

zip train_healthy_pancreas.zip healthy_pancreas_*.jpg
cp  train_healthy_pancreas.zip  th.zip

zip train_pancreatic_cancer.zip pancreatic_cancer_*.jpg
cp  train_pancreatic_cancer.zip  tc.zip

