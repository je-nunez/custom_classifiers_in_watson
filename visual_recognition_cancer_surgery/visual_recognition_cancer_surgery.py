#!/usr/bin/env python2.7
# coding=utf-8

# pylint: disable=line-too-long

"""
Watson as Fellow of the Royal College of Surgeons.

--- Expected Inputs and Results:

Receives as input some visual samples of cancer on a real human tissue and
dissects the main human organ of which these visual samples belong to find
the regions of this organ which have signs of cancer, and find the geometrical
countours (edges) of this cancer in that organ, in the same way that a surgeon
needs to do an optical recognizance of the organ to know on which countours
(edges) to introduce the scalpel and start removing.

It works by constructing a new visual classifier with Watson Visual
Recognition to know what is visually cancer (and needs to be extirpated)
and what is not.

As sample input, it takes this picture of a human organ (a liver from
primary cancer of the pancreas):

https://commons.wikimedia.org/wiki/File:Secondary_tumor_deposits_in_the_liver_from_a_primary_cancer_of_the_pancreas.jpg

and some positive and negative visual biopsias of it as samples of what
is visually cancer and what is not. This figure and small subsamples are
already downloaded in the ./pancreatic/ subfolder here, with filename
'cl.jpg' for the main picture of the liver, and a few sections in that
subfolder like 'healthy_pancreas_*.jpg' and 'pancreatic_cancer_*.jpg' to
build the new Watson classifier as to what is visually cancer (both are
grouped in two zip files, 'th.zip' and 'tc.zip', with these visual
subsamples of what looks healthy and what looks cancerigenous.

--- How it Works:

The program receives as a command-line argument the number of subtiles
into which to slice the image of the human organ -the liver in this case-
and then, once that Watson has been trained in the new visual classiffier,
slices the image into those many subtiles, creates a new temporary zip
archive with all these generated subtiles, and submits it to Watson to be
classified.

Once Watson answers with the score of each dinamycally sliced subtile,
then the program masks the color of those subtiles which are healthy
(Watson score = 0) and for all the others subtiles, lights or shades them
according to the score Watson returned for them, using the alpha composite
of the image.

It then rejoins all the subtiles back into the image of the organ with
only cancerigenous subtiles clearly shown, and then finds the countours
(edges) of these visually cancerigenous zones in the human organ.

It then deletes all dinamycally generated slices and zip file used for
this analysis.

--- Contraints:

The execution-time seems to be linear in respect to the number of subtiles
into which to slice the image of the human organ, and TCP timeouts with
Bluemix are possible. Optimistic execution-time is about a few minutes,
but I have seen executions times of several hours followed by a TCP
timeout. (The number of subtiles is quadratic, so when you ask to slice the
visual image of the organ in 40 subtiles, the program slices the image
horizontally and vertically in 40x40 = 1600 subtiles with identical
dimensions, which are then submitted via a temporary zip file into Watson. If
the program is requested to slice the image in 50 subtiles, it sends 2500
subtiles which equal dimensions to Watson. Of course, more subtiles is a
finer slicing and a better precision for the countours (edges) advised to the
surgeon, who do get tired in long surgeries. -When we refer to the number of
subtiles we refer to the number after slicing, which is the square of the
input parameter 'number_of_subtiles'-.

The maximum limit of subtiles is 99, ie., 99 x 99 subtiles sent to Watson.
This is a limitation of this client program, not of Watson.

The program works on a static image of a human organ, and, since it slices
it and sends them to a neural network for classification, then it needs
high-resolution images, otherwise the sliced subtiles would have too few
pixels is the image of the organ is in low resolution. In our case, the
image of the liver is 2,784 × 1,884 pixels (I haven't tried 99x99 slicing
it, too slow).

--- Feature requests:

Since all the sliced subtiles are sent at once to Watson, a feature would
be to have each subtile classified in parallel by an independent neural
network, and this would cut the execution time of the program. (The hyper-
paramenters, axons' weights, neurons biases, etc, can be learned by training
a 'leader' neural network with the same architecture, which is the one that
initially is trained under the positive and negative examples of the new
visual classifier. Once this leader has learned this new classifier of what
is cancer, then this 'leader' neural network is cloned into 99x99 identical
ones to classify in parallel each actual received subtile.

Another feature (this is less of a feature and probably more difficult) is
that the image slicing and contours analysis be done in the server-side,
i.e., the client sends to Bluemix the source image of the human organ and the
number of subtiles it needs and then the server slices it, classifies each
subtile, repaints them according to the classification score and joins them
back in a resulting image of the organ. In this case, the client is more
passive. (The idea of this feature is that, by incorporating this
functionality, the server should do contour (edge) analysis of those sections
of the image where the classifier changes in value, so the client only asks
"answer me the contours of this classifier". This is less difficult than
it sounds since countour -edge- analysis is lineal algebra if it is done
pixel-by-pixel; see, for example, the 3x3 lineal matrices used in the Python
Imaging Library -PIL: Pillow should also use them- to find contours and
edges:

    find the 3x3 matrix below "class FIND_EDGES(BuiltinFilter):":

    http://www.sourcecodebrowser.com/python-imaging/1.1.7/_image_filter_8py_source.html

Of course, this is to find edges in pixels, where no classification is
needed: Watson can recognize different adjacent zones -by doing clustering of
similar pixels together-, and then, once the adjacent zones are found, apply
the 3x3 lineal matrix to find edges and contours of those zones, not pixels.
)

This is also can be used in mobile phones, because these devices require
real-time answers, a phone can't be left waiting.

--- Other uses:

In a similar way as the image of an organ is sliced and then scored by a
custom neural network classifier, the image of an ecological disaster can be
sliced into several subtiles to know which containment measures to take on
each subtile. For example, in oil spills:

   http://www.treehugger.com/environmental-policy/us-not-prepared-major-oil-spill-arctic.html

Scoring each subtile for directing the positioning of bioremediants,
dispersants, or the positioning or more appropiate techniques, by combining
also sensors which predict the direction and force of the water surface flows,
etc. (The later falls more properly in Tradeoff-Analytics than in visual
recognition since these sensors can give the speed of the water flow in each
GPS location, etc. For example, the National Oceanic and Atmospheric
Administration has a system that models how different types of oil undergo
physical and chemical changes in the marine environment:

   http://response.restoration.noaa.gov/adios

but this system can not locate visually each of these different types of oils
in an actual spill. If images with sufficient resolution is feed into Watson,
probably it could locate where each type of oil and where it is being led by
the currents.

In a similar ecological case, it is to deal with consumism. Matter is not
created nor destroyed, but transformed, so an increase in consumism means
more difficulty in properly and economically separating the residues of this
consumism, e.g., economically classifying aluminium from copper from plastic,
etc., by using the visual refraction indices at a short distance. This is a
similar visual classification as done above. Because, as consumism increases,
then promptly and easily classifying its residues is both an economical desire
to re-use without using non-renovable sources, and ecological as well, because
it pre-empts artificial micro-environments by a rapid classification and
re-use.

--- Copyright, Jose Emilio Nunez-Mayans, 2016
"""

import sys
import os
import logging
from zipfile import ZipFile

sys.path.append('/Library/Python/2.7/site-packages/')

# These two Python modules need to be installed from the PyPI, e.g., by
# pip install image_slicer

from PIL import Image

import image_slicer

from watson_developer_cloud import VisualRecognitionV2Beta as VisualRecognition
from watson_developer_cloud.watson_developer_cloud_service \
     import WatsonException


class Config(object):
    """
       A few globals variables used by the program, represented as static
       field members of this Python class.
    """

    # pylint: disable=too-few-public-methods

    # the number of subtiles in which to slice the image of the human liver.
    # This value can be changed by the {-n|--number_subtiles} command-line
    # option. Recall the actual number of subtiles that are sent to Watson
    # to be classified as to whether they are visually cancerigenous or not
    # is the square of this value.
    number_of_subtiles = 20

    # the maximum number of tiles that the "image_slicer" Python module
    # supports (see this module's "main.py" file, the constant TILE_LIMIT
    # ( = 99 * 99 )
    MAXIMUM_TILES = 99


def main():
    """Main function."""

    import argparse
    from argparse import RawDescriptionHelpFormatter

    detailed_usage = get_this_script_docstring()

    summary_usage = 'Tries to advice a surgeon by finding the countours of ' \
                    'a pancreatic cancer that has expanded to the liver.'

    parser = argparse.ArgumentParser(description=summary_usage,
                                     epilog=detailed_usage,
                                     formatter_class=\
                                                  RawDescriptionHelpFormatter)

    parser.add_argument('-n', '--number_subtiles',
                        default=Config.number_of_subtiles,
                        required=False, type=int, metavar='number_of_subtiles',
                        help='Number of subtiles in which to divide the liver '
                             'to find which one(s) has visual traces of '
                             'cancerigenous tumours to extirpate surgically'
                             '(default: "%(default)d")')

    parser.add_argument('-u', '--username',
                        required=True, type=str, metavar='username',
                        help='The Bluemix username to connect to the Watson '
                             'Visual Recognizer front-end')

    parser.add_argument('-p', '--password',
                        required=True, type=str, metavar='password',
                        help='The Bluemix password to connect to the Watson '
                             'Visual Recognizer front-end')

    args = parser.parse_args()

    # configure logging ('level' should have been taken from the command-line)
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.debug("Called with number_subtiles: %d, user: %s, passwd %s",
                  args.number_subtiles, args.username, args.password)

    if not 1 <= args.number_subtiles <= Config.MAXIMUM_TILES:
        sys.stderr.write("Not a valid number of subtiles, {}".
                         format(args.number_subtiles))
        sys.exit(1)

    Config.number_of_subtiles = args.number_subtiles

    # Obtain your credentials using:
    #    `cf env <the-name-of-my-watson-bluemix-visual-recognition-app>`
    visual_recogn = VisualRecognition(version='2015-12-02',
                                      username=args.username,
                                      password=args.password)

    # Build a new visual classifier for this cancer
    new_classifiers = train_new_cancer_classifier(visual_recogn)

    # Slice, classify the subtiles, and repaint the organ according to
    # that new visual classifier Watson has been trained on
    classify_and_repaint_organ(visual_recogn, new_classifiers)


def train_new_cancer_classifier(visual_recogn):
    """
       Trains the neural network for visual recognizing a new cancer.
       This is the first step in the program.

       Returns the new classifier_id learned by Watson.
    """
    logging.debug("Training Watson to recognize visually this cancer")

    with open('pancreatic/tc.zip', 'rb') as train_positive, \
         open('pancreatic/th.zip', 'rb') as train_negative:

        new_classif = visual_recogn.create_classifier(name='pc_demo',
                                                      positive_examples=train_positive,
                                                      negative_examples=train_negative)

    # From the command-line (curl) the `create_classifier` returns a JSON like:
    # (watson_developer_cloud.VisualRecognition returns a Python's dictionary)
    #
    # {
    #   "name" : "pc_demo",
    #   "classifier_id" : "pc_demo_223473753",
    #   "created" : "2016-02-10T02:45:09.000Z",
    #   "owner" : "d36d919a-d2ca-417c-8823-924c40c0603a-us-south"
    # }
    interesting_classes = [new_classif['classifier_id']]  # only one class, in
                                                          # theory can have
                                                          # multiple classes
    logging.info("Watson learned a new visual classifier for this cancer: %s",
                 repr(interesting_classes))

    return interesting_classes


def classify_and_repaint_organ(visual_recogn, on_classifiers):

    """
       Slice the image of the organ in different subtiles (contours in
       general), and then upload them to Watson for classification as what is
       the score of these subtiles showing the surface of cancer.
    """

    number_subtiles = Config.number_of_subtiles * Config.number_of_subtiles
    logging.debug("Slicing the image of the human organ into %d subtiles",
                  number_subtiles)
    contours = image_slicer.slice('pancreatic/cl.jpg', number_subtiles,
                                  save=True)
    logging.debug("Done. Creating a new ZIP archive with these subtiles")
    with ZipFile('pancreatic/lineal_countour.zip', 'w') as contours_zip:
        for tile in contours:
            contours_zip.write(tile.filename)
            os.remove(tile.filename)

    logging.info("Submitting ZIP archive to Watson's Visual Recognition")
    with open('pancreatic/lineal_countour.zip', 'rb') as countours_archive:
        classification = visual_recogn.classify(images_file=countours_archive,
                                                classifier_ids=on_classifiers)

    logging.info("Watson classified the subtiles of the organ")
    os.remove('pancreatic/lineal_countour.zip')    # delete the zip

    repaint_contours_by_classifier(classification, contours)


def repaint_contours_by_classifier(classification, img_contours):
    """This depends if the classifier is stable, ie., robust."""

    logging.debug("Starting to repaint the organ by subtiles score on cancer")

    for subtile in classification["images"]:
        visual_chrome = 0
        if "scores" not in subtile:
            # It hasn't been scored
            visual_chrome = 0
        else:
            # the color is precisely the statistical score of this tile
            # retorned by the Watson classifier (sum() below is not needed,
            # indeed there is only one classifier and one score, not multiple
            # classifiers, as in the case if there were multiple cancers.)
            visual_chrome = sum([poly_chrome["score"]
                                 for poly_chrome in subtile["scores"]])

        # Verify: Try to find this classified 'subtile["image"]' retuned from
        # Watson in the original set of countours
        original_img = None
        for original_contour in img_contours:
            if original_contour.filename == subtile["image"]:
                original_img = original_contour.image
                break

        if not original_img:
            logging.critical("Couldn't find %s in original countours",
                             subtile["image"])
            return

        logging.debug("Subtile %s to be repainted with alpha %f",
                      subtile["image"], 1 - visual_chrome)

        new_img = Image.new(mode='RGBA',
                            size=original_img.size,
                            color=(0, 0, 0, int(255 * (1 - visual_chrome))))
        original_img.paste(new_img, (0, 0), new_img)
        logging.debug("Subtile %s repainted with alpha %f",
                      subtile["image"], 1 - visual_chrome)

    logging.info("Joining together all the repainted subtiles in the organ")
    watson_vision = image_slicer.join(img_contours)

    logging.info("Joined back. Saving the repainted image of the organ")
    watson_vision.save('result.png')
    logging.info("Repainted image of the organ saved in './result.png'")


def delete_custom_classifiers(custom_watson_classifiers, visual_recogn):

    """Delete the custom visual classifiers we created in Watson."""

    for new_class in custom_watson_classifiers:
        res = ''
        try:
            res = visual_recogn.delete_classifier(classifier_id=new_class)
        except WatsonException as exc:
            # The service currently has a bug where even successful deletions
            # return a 404
            print repr(exc)

        print repr(res)


def get_this_script_docstring():
    """Utility function to get the Python docstring of this script"""
    import inspect

    current_python_script_pathname = inspect.getfile(inspect.currentframe())
    dummy_pyscript_dirname, pyscript_filename = \
                os.path.split(os.path.abspath(current_python_script_pathname))
    pyscript_filename = os.path.splitext(pyscript_filename)[0]  # no extension
    pyscript_metadata = __import__(pyscript_filename)
    pyscript_docstring = pyscript_metadata.__doc__
    return pyscript_docstring


if __name__ == '__main__':
    main()
